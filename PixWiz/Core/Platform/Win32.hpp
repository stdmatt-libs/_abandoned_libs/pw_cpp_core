//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Win32.hpp                                                     //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 18, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#if PW_OS_WINDOWS
    #define WIN32_LEAN_AND_MEAN
    #define _WINSOCKAPI_
    #include <Windows.h>


    #undef SetCurrentDirectory
#else
    #error "Should include Base/Discovery/OS.hpp and this file should be" \
           "included only by files that builds on Windows"
#endif // PW_OS_WINDOWS
