// Header
#include "CmdLine/Parser.hpp"
// PixWiz - Core
#include "String/Str.hpp"
#include "Algo/Algo.hpp"


namespace pw { namespace CmdLine {

//----------------------------------------------------------------------------//
// Helper Methods                                                             //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
bool
_IsShortArg(char const * const v)
{
    return Str::Len(v) > 1
        && v[0] == '-'
        && v[1] != '-';
}

//------------------------------------------------------------------------------
bool
_IsLongArg(char const * const v)
{
    return Str::Len(v) > 2
        && v[0] == '-'
        && v[1] == '-';
}


//----------------------------------------------------------------------------//
// Implementation Methods                                                     //
//----------------------------------------------------------------------------//
//
// ParseError
//
char const * const
ParseError::DefaultMessage(ParseError const &error)
{
    if(error.type == ErrorType::InvalidFlag) {
        return Str::FormatAlloc("Flag (%s) not recognized.", error.arg);
    } else if(error.type == ErrorType::InvalidArgument) {
        return Str::FormatAlloc(
            "Flag (%s) doesn't accept value (%s)",
            ((error.flag->long_name) ? error.flag->long_name : error.flag->short_name),
            error.arg
        );
    } else /* ErrorType::MissingArgument */ {
        return Str::FormatAlloc(
            "Flag (%s) requires argument.",
            ((error.flag->long_name) ? error.flag->long_name : error.flag->short_name)
        );
    }
}

//
// Parser
//
//------------------------------------------------------------------------------
void
Parser::Parse(
    u32         argc,
    char const *argv[],
    ParserOnErrorCallback_t &&on_error_callback)
{
    Flag *_processing_flag = nullptr;

    for(u32 i = 1; i < argc; ++i) {
        char *curr_arg      = const_cast<char*>(argv[i]);
        bool const is_short = _IsShortArg(curr_arg);
        bool const is_long  = _IsLongArg (curr_arg);

        //
        // Process non arg
        if(!is_short && !is_long) {
            // Easy case - positional argument...
            if(!_processing_flag) {
                _positional_args.push_back(curr_arg);
                continue;
            }

            // We have a flag that is being processed so this argument belongs to it.
            _processing_flag->value = curr_arg;
            if(_processing_flag->found_callback) {
                bool valid = _processing_flag->arg_callback(curr_arg);
                if(!valid) {
                    on_error_callback(
                        ParseError(
                            ErrorType::InvalidArgument,
                            _processing_flag,
                            curr_arg
                        )
                    );
                    return;
                }
            }
            // Finished with this flag...
            _processing_flag = nullptr;
        }
        //
        // Process flag
        else {
            Flag const *_curr_flag = (is_short)
                ? GetFlagWithShortName(curr_arg)
                : GetFlagWithLongName (curr_arg);

            // Invalid flag name.
            if(!_curr_flag) {
                on_error_callback(ParseError(ErrorType::InvalidFlag, nullptr, curr_arg));
            }

            // We have a flag being processed but we found another one,
            // this means that - If the argument is optional we finished
            // if that flag, if the argument is required we are missing it.
            if(_processing_flag) {
                if(_processing_flag->arg_type == Args::Optional) {
                    _processing_flag = nullptr;
                } else {
                    on_error_callback(
                        ParseError(ErrorType::InvalidFlag, _processing_flag, curr_arg)
                    );
                }
            }

            // Inform that flag was found...
            if(_curr_flag->found_callback) {
                _curr_flag->found_callback();
            }

            // If the art_type for the current flag is No, means that
            // whatever comes next we don't mind about this flag anymore...
            _processing_flag = (_curr_flag->arg_type == Args::No)
                ? nullptr
                : const_cast<Flag*>(_curr_flag);
        }
    }

    // Finished with args but we have a lasting flag. that requires args..
    if(_processing_flag && _processing_flag->arg_type == Args::Required) {
        on_error_callback(
            ParseError(ErrorType::InvalidFlag, nullptr, nullptr)
        );
    }
}



//------------------------------------------------------------------------------
Flag const*
Parser::GetFlagWithShortName(char const * const short_name) const
{
    auto it = pw::Algo::FindIf(_flags, [short_name](Flag const &f) {
        return Str::Equals(f.short_name, short_name+1);
    });

    if(it == std::end(_flags)) {
        return nullptr;
    }

    return &(*it);
}


//------------------------------------------------------------------------------
Flag const*
Parser::GetFlagWithLongName(char const * const long_name ) const
{
    auto it = pw::Algo::FindIf(_flags, [long_name](Flag const &f) {
        return Str::Equals(f.long_name, long_name+2);
    });

    if(it == std::end(_flags)) {
        return nullptr;
    }
    return &(*it);
}

} // namespace CmdLine
} // namespace pw
