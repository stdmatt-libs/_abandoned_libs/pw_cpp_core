//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Parser.hpp                                                    //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 12, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

// std
#include <initializer_list>
#include <functional>
#include <vector>
// PixWiz
#include "Base/Base.hpp"

namespace pw { namespace CmdLine {

enum class Args : u8 {
    No,
    Required,
    Optional
}; // enum Args

struct Flag {
    typedef std::function<void ()>                   FlagFoundCallback_t;
    typedef std::function<bool (char const * const)> FlagArgumentCallback_t;

    Flag(
        char const * const    long_name,
        char const * const    short_name,
        Args                  arg_type,
        FlagFoundCallback_t    &&found_callback = nullptr,
        FlagArgumentCallback_t &&arg_callback   = nullptr)
        : long_name     (long_name)
        , short_name    (short_name)
        , arg_type      (arg_type)
        , found_callback(std::move(found_callback))
        , arg_callback  (std::move(arg_callback))
    {
        // Empty...
    }

    char const * const  long_name;
    char const * const  short_name;

    Args const arg_type;

    FlagFoundCallback_t    found_callback;
    FlagArgumentCallback_t arg_callback;

    char const * value = nullptr;
}; // struct Flag


enum ErrorType {
    InvalidFlag,
    MissingArgument,
    InvalidArgument,
}; // enum ErrorType

struct ParseError {
    ErrorType    const type;
    Flag const * const flag;
    char const * const  arg;

    ParseError(
        ErrorType          type,
        Flag const * const flag,
        char const * const arg)
    : type(type)
    , flag(flag)
    , arg (arg)
    {
        // Empty...
    }


    static char const * const DefaultMessage(ParseError const &error);
}; // struct ParseError




class Parser
{
    //
    // Enums / Constants / Typedefs
    //
public:
    typedef std::vector<Flag>   FlagsContainer_t;
    typedef std::vector<char *> PositionalArgsContainer_t;

    typedef std::function<void (ParseError &&)> ParserOnErrorCallback_t;


    //
    // Public Methods
    //
public:
    void Parse(
        u32         argc,
        char const *argv[],
        ParserOnErrorCallback_t &&on_error_callback);


    void
    AddFlags(FlagsContainer_t &&flags)
    {
        _flags = std::move(flags);
    }

    Flag const* GetFlagWithShortName(char const * const short_name) const;
    Flag const* GetFlagWithLongName (char const * const long_name ) const;

    const FlagsContainer_t &
    GetFlagsArgs() const
    {
        return _flags;
    }

    const PositionalArgsContainer_t &
    GetPositionalArgs() const
    {
        return _positional_args;
    }


    //
    // iVars
    //
private:
    FlagsContainer_t          _flags;
    PositionalArgsContainer_t _positional_args;

}; // class Parser

} // namespace CmdLine
} // namespace pw
