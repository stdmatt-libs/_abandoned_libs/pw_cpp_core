//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Defer.hpp                                                     //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 18, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once
// std
#include <functional>
// PixWiz - Core
#include "Base/Base.hpp"

namespace pw { namespace Detail
{

struct _Defer {
    PW_DISALLOW_COPY_CTOR_AND_COPY_ASSIGN(_Defer);
    PW_DISALLOW_MOVE_CTOR_AND_MOVE_ASSIGN(_Defer);

    typedef std::function<void()> Function_t;
    _Defer(Function_t &&f) : _function(std::move(f)) {};
    ~_Defer() { _function(); }

    Function_t _function;
}; // struct _Defer

} // namespace Detail
} // namespace pw


#define PW_DEFER(_lambda_) \
    pw::Detail::_Defer PW_EVALUATOR(_dont_shoot_yourself_on_the_foot_, __LINE__)((_lambda_))

#define PW_DEFER_FREE(_mem_) \
    PW_DEFER([_mem_](){ pw::Memory::Free(_mem_); })
