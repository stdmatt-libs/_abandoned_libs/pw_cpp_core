//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Print_Detail.hpp                                              //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 12, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

// std
#include <stdio.h>
#include <string>
// PixWiz - Core
#include "Base/Base.hpp"

namespace pw { namespace Detail {

//----------------------------------------------------------------------------//
//   This will let us to print std::string directly from the %s without       //
//   the need to call the c_str() by hand.                                    //
//   This works because the arguments expansion on pw::Print calls those      //
//   functions for each argument passed to it.                                //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
template <typename T>
inline const T
_Print_Argument(T value)
{
    return value;
}

//------------------------------------------------------------------------------
template <typename T>
inline T const *
_Print_Argument(std::basic_string<T> const &value)
{
    return value.c_str();
}


//----------------------------------------------------------------------------//
// printf-like Functions                                                      //
//    Private implementation                                                  //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
template <typename ... Args>
inline void
_Print(std::string const &format, Args const & ... args)
{
    // @XXX: ignoring the  warning: format not a string literal and no format arguments [-Wformat-security]
#if PW_COMPILER_IS_GCC || PW_COMPILER_IS_CLANG
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-security"
#endif
    ::printf(format.c_str(), pw::Detail::_Print_Argument(args)...);

#if PW_COMPILER_IS_GCC || PW_COMPILER_IS_CLANG
#pragma GCC diagnostic pop
#endif
}

} // namespace Detail
} // namespace pw
