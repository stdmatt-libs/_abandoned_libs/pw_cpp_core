// Header
#include "IO/Path.hpp"
// @incomplete - Split to other platforms...
// PixWiz - Core
#include "String/String.hpp"

namespace pw {

//
//
//
//------------------------------------------------------------------------------
std::string
Path::Abs(std::string const &path)
{
    // @notice: Trying to follow the implementation of:
    //   /usr/lib/python2.7/posixpath.py
    if(Path::IsAbs(path)) {
        return Path::Normalize(path);
    }

    return Path::Normalize(Path::Join(Path::CurrentDirectory(), path));
}

//------------------------------------------------------------------------------
std::string
Path::Normalize(std::string path, bool force_forward_slash /* true */)
{
    //@note(stdmatt): Following the implementation of:
    //  /usr/lib/../a/b/.././../a/b/././python2.7/posixpath.py
    if(path.empty()) {
        return ".";
    }

    StringUtils::ReplaceInplace(path, "\\", "/");
    auto components = StringUtils::Split(path, "/");

    // Start iterating without the root component if it exits.
    // Only in Windows paths since it includes the drive letter.
    //   Windows: C://druid/games
    //   Unix   : /druid/games
    auto abs         = Path::IsAbs(path);
    auto is_unix_abs = abs && components.front() == "/";
    auto comp_it     = std::begin(components);
    if(abs && !is_unix_abs) {
        ++comp_it;
    }

    // Iterate for all components, at end the comps_stack
    // will have a normalized path items.
    std::vector<std::string> comps_stack;
    comps_stack.reserve(components.size());

    for(;comp_it != std::end(components); ++comp_it) {
        auto const &comp = *comp_it;
        if(comp == ".") {  // Same directory, just ignore.
            continue;
        } else if(comp == "..") {
            if(!comps_stack.empty()) {  // No more dirs to go back.
                comps_stack.push_back(comp);
            } else {  // Can go back a directory.
                comps_stack.pop_back();
            }
        } else { // Normal directory component.
            comps_stack.push_back(comp);
        }
    }

    // Build the normalized path.
    auto sep         = Path::GetSeparator();
    auto replace_sep = (force_forward_slash) ? "/" : sep;

    std::stringstream ss;
    // Insert the root if is an abs path.
    if(abs) {
        if(is_unix_abs) {  // Insert the root: /
            ss << replace_sep;
        } else { // Insert the drive letter: C:
            ss << components.front() << replace_sep;
        }
    }

    auto size = comps_stack.size();
    for(int i = 0; i < size; ++i) {
        ss << comps_stack[i];
        if(i < (size -1)) {
            ss << replace_sep;
        }
    }

    return ss.str();
}


//
// Path Components
//
//------------------------------------------------------------------------------
std::string
Path::Basename(std::string const &path)
{
    // @performance: Can be done without calling Split

    //@note(stdmatt): Trying to follow:
    //  /usr/lib/python2.7/posixpath.py
    return Path::Split(path)[1];
}

//------------------------------------------------------------------------------
std::string
Path::Dirname(std::string const &path)
{
    // @performance: Can be done without calling Split

    //@note(stdmatt): Trying to follow:
    //  /usr/lib/python2.7/posixpath.py
    return Path::Split(path)[0];
}

//------------------------------------------------------------------------------
std::array<std::string, 2>
Path::Split(std::string const &path)
{
    auto const &sep   = Path::GetSeparator();
    auto        index = path.rfind(sep);

    if(index == std::string::npos) {
        return {"", path};
    }

    auto const offset = (index == 0 && path[0] == sep[0]) ? 1 : 0;
    return {
        path.substr(0, index + offset),
        path.substr(index + 1)
    };
};


//
// Extension
//
//------------------------------------------------------------------------------
std::string
Path::GetExtension(std::string const &path)
{
    // @performance: Can be done without calling SplitExtension.
    return SplitExtension(path)[1];
}

//------------------------------------------------------------------------------
std::string
Path::ChangeExtension(std::string const &path, std::string const &new_extension)
{
    // @performance: Can be done without calling SplitExtension.
    if(path.empty()) {
        return path;
    }

    auto split = Path::SplitExtension(path);

    if(new_extension.empty()  ) { return split[0];                       } // Remove extension.
    if(new_extension[0] != '.') { return split[0] + "." + new_extension; } // Add extension with dot.

    return split[0] + new_extension; // Add extension (already with dot)
}

//------------------------------------------------------------------------------
std::string
Path::RemoveExtension(std::string const &path)
{
    return Path::SplitExtension(path)[0];
}

//------------------------------------------------------------------------------
std::array<std::string, 2>
Path::SplitExtension(std::string const &path)
{
    //@note(stdmatt): Trying to follow:
    //  /usr/lib/python2.7/genericpath.py
    auto last_dot_index = path.rfind('.');

    //There's no dots on path
    //  So there's no extension separators.
    if(last_dot_index == std::string::npos) {
        return {path, ""};
    }

    // @Incomplete: On windows we don't look for backslashes...
    auto const &separator     = Path::GetSeparator();
    auto const last_sep_index = path.rfind(separator[0]);

    // The last dot happened not into final part of path.
    if(last_dot_index < last_sep_index &&
       last_sep_index != std::string::npos)
    {
        return {path, ""};
    }

    auto const filename_index = (int)last_dot_index - (int)last_sep_index -1;
    if(filename_index <= 0) {
        return {path, ""};
    }

    auto const base = path.substr(0, last_dot_index);
    auto const ext  = path.substr(last_dot_index + 1);
    return {base, ext};
};


} // namespace pw
