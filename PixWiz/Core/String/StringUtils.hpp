//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : StringUtils.hpp                                               //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 18, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once
// std
#include <string>
#include <vector>
// PixWiz - StringUtils Details
#include "String/Detail/StringUtilsDetail.hpp"

namespace pw { namespace StringUtils {

inline void
ReplaceInplace(
    std::string       &str,
    std::string const &what,
    std::string const &to)
{
    // Keep searching and replacing the contents of string
    // until we haven't anything to Replace.
    while(true) {
        auto index = str.find(what);

        // We haven't found anything so there's no
        // meaning to continue searching.
        if(index == std::string::npos) {
            break;
        }

        str.replace(index, what.size(), to);
    }
}


template <typename T, typename... Args>
std::string
Join(std::string const &separator, const T &first,  Args... args)
{
    using namespace pw::StringUtils::Detail_Join;
    return Join(separator, first) + Join(separator, args...);
}


inline std::vector<std::string>
Split(
    std::string const &str,
    std::string const &chars,
    size_t             max_splits = 0)
{
    auto vec = std::vector<std::string>();
    vec.reserve(str.size() / 2);

    size_t index       = 0;
    size_t new_index   = 0;
    size_t curr_Splits = 0;
    while(true) {
        new_index = str.find_first_of(chars, index);

        auto sub_str = str.substr(index, new_index - index);
        vec.push_back(sub_str);

        if(new_index == std::string::npos) {
            break;
        } else {
            index = new_index + 1;
            ++curr_Splits;

            // Max Splits reached - Get out.
            if(max_splits > 0 && curr_Splits >= max_splits) {
                break;
            }
        }
    }

    return vec;
}

inline void
TrimInplace(
    std::string       &str,
    std::string const &chars = " ")
{
    if(str.empty()) {
        return;
    }

    auto begin = str.find_first_not_of(chars);
    if(begin == std::string::npos) {
        str = "";
        return;
    }

    auto end = str.find_last_not_of(chars);
    if(end == std::string::npos) {
        str = "";
        return;
    }

    str = str.substr(begin, end-begin +1);
}

inline std::string
Trim(
    std::string const &str,
    std::string const &chars = " ")
{
    auto s = str;
    TrimInplace(s, chars);
    return s;
}


inline void
InsertAtInplace(
    size_t             index,
    std::string const &value,
    std::string       &str)
{
    str.insert(index, value);
}




} // namespace StringUtils
} // namespace pw
