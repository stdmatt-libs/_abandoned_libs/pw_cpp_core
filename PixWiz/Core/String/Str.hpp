//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Str.hpp                                                       //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 12, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

// std
#include <string.h>
#include <stdlib.h>
// PixWiz - Core
#include "Base/Base.hpp"
#include "Memory/Memory.hpp"
// PixWize - Detail
#include "String/Detail/asprintf.hpp"


namespace pw { namespace Str {

inline bool
IsNum(char const c)
{
    return isdigit(c);
}

inline bool
IsAlphaNum(char const c)
{
    return isalnum(c);
}


inline bool
ParseU8(u8 *out, char const * const value)
{
    // @TEMPORARY....
    i32 a = atoi(value);
    if(a > pw::MaxValue<u8>()) {
        return false;
    }

    *out = (u8)a;
    return true;
}

inline char *
ToUpper(char * const str)
{
    char *c = str;
    while(c && *c != '\0') {
        *c = toupper(*c);
        ++c;
    }
    return str;
}

inline u32
Len(char const * const str)
{
    return strlen(str);
}

inline bool
Equals(char const * const a, char const * const b, u32 size = 0)
{
    if(size == 0) {
        return strcmp(a, b) == 0;
    } else {
        auto v = strncmp(a, b, size);
        return v == 0;;
    }
}

inline bool
IsEmpty(char const * const str)
{
    return Str::Len(str) == 0;
}

inline const char*
Search(char const *haystack, char const *needle)
{
    return strstr(haystack, needle);
}

inline char const *
Trim(char const * const str, char to_strip = ' ')
{
    char *stripped = const_cast<char*>(str);
    u32 s = 0;
    u32 e = Str::Len(str);
    bool mod = true;
    while(mod) {
        mod = false;
        if(stripped[s] == to_strip) {
            ++s;
            mod = true;
        }
        if(stripped[e] == to_strip) {
            --e;
            mod = true;
        }
    }

    stripped[e] = '\0';
    stripped    = (stripped + s);

    return stripped;
}

inline char *
Copy(char const * const str)
{
    return strdup(str);
}

template <typename... Args>
char const * const
FormatAlloc(char const * const format, Args ... args)
{
    if(sizeof...(args) == 0) {
        return Str::Copy(format);
    }

    // @incomplete: not checking errors...
    char *buf = nullptr;
    Detail::_asprintf(&buf, format, args ...);

    return buf;
}


template <typename Container>
char const *
Join(char const * const separator, Container const &container)
{
    u32 buff_size   = 0;
    u32 items_count = 0;
    for(auto const &item : container) {
        buff_size += strlen(item);
        ++items_count;
    }

    u32   sep_len = strlen(separator) ;
    u32   offset  = 0;
    char *buffer  = pw::Memory::Alloc<char>(buff_size + (sep_len * items_count) + 1);
    for(auto const &item : container) {
        u32 item_len = strlen(item);
        memcpy(buffer + offset, item, item_len);
        offset += item_len;
        if(sep_len != 0) {
            memcpy(buffer + offset, separator, sep_len);
            offset += sep_len;
        }
    }

    buffer[offset] = '\0';
    return buffer;
}

} // namespace Str
} // namespace pw
