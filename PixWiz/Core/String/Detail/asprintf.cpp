//  Description :                                                             //
//    asprintf(3) function implementation - Based on work from:               //
//    Copyright (C) 2014 insane coder (http://insanecoding.blogspot.com/,     //
//                                     http://asprintf.insanecoding.org/)     //
//                                                                            //
//    Permission to use, copy, modify, and distribute this software for any   //
//    purpose with or without fee is hereby granted, provided that the above  //
//    copyright notice and this permission notice appear in all copies.       //
//                                                                            //
//    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIE //
//    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF        //
//    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR //
//    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  //
//    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN   //
//    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF //
//    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.          //
//---------------------------------------------------------------------------~//

// Header
#include "asprintf.hpp"
// std
#include <limits.h>
#include <stdio.h>
// PixWiz - Core
#include "Memory/Memory.hpp"

namespace pw { namespace Detail {

//------------------------------------------------------------------------------
int
_asprintf(char **strp, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
        auto r = _vasprintf(strp, fmt, ap);
    va_end(ap);

    return(r);
}

//------------------------------------------------------------------------------
int
_vasprintf(char **strp, const char *fmt, va_list ap)
{
    va_list ap2;
    va_copy(ap2, ap);

    // Calculate how much space we needed on this buffer.
    auto size_needed = vsnprintf(nullptr, 0, fmt, ap2);
    auto result      = -1;

    // #define   = false
    if((size_needed >= 0) && (size_needed < INT_MAX)) { // Ok, we have a valid size.
        *strp = pw::Memory::Alloc<char>(size_needed + 1); // +1 for null

        if(*strp) { // We could allocate memory...
            auto chars_written = vsnprintf(*strp, size_needed+1, fmt, ap); // +1 for null
            if(chars_written < 0) { // Error...
                // @todo(stdmatt): How handle errors here??
                pw::Memory::Free(*strp);
            } else {
                result = chars_written;
            }
        }
    } else { // Ops.. we don't have a valid size.
        // @todo(stdmatt): How handle errors here???
        *strp = nullptr;
    }

    va_end(ap2);
    return result;
}

} // namespace Detail
} // namespace pw
