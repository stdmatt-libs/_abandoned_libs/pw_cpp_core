//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : PointerUtils.hpp                                              //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 19, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

// std
#include <memory> // Smart Pointers ;D

//----------------------------------------------------------------------------//
// Shared Pointer                                                             //
//----------------------------------------------------------------------------//
///-----------------------------------------------------------------------------
/// @brief Declares a Shared-Pointer of type (_type_) named SPtr.
#define PW_SHARED_PTR_OF(_type_) \
    typedef std::shared_ptr<_type_> SPtr


//----------------------------------------------------------------------------//
// Unique Pointer                                                             //
//----------------------------------------------------------------------------//
///-----------------------------------------------------------------------------
/// @brief Declares an Unique-Pointer of type (_type_) named UPtr.
#define PW_UNIQUE_PTR_OF(_type_) \
    typedef std::unique_ptr<_type_> UPtr

///-----------------------------------------------------------------------------
/// @brief
///    Declares an Unique-Pointer with a custom deleter of
///    type (_type_) named UPtr.
#define PW_UNIQUE_PTR_DELETER_OF(_type_, _func_) \
    typedef std::unique_ptr<_type_, _func_> UPtr;


//----------------------------------------------------------------------------//
// All Smart Pointers.                                                        //
//----------------------------------------------------------------------------//
///-----------------------------------------------------------------------------
/// @brief
///    Declares both Shared and Unique pointers of type (_type_)
///    named SPtr and UPtr.
#define PW_SMART_PTRS_OF(_type_) \
    PW_SHARED_PTR_OF(_type_);    \
    PW_UNIQUE_PTR_OF(_type_)
