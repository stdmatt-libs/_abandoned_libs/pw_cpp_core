//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Result.hpp                                                    //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 19, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once


namespace pw {

template <typename Type>
class Result
{
    static Result<Type> Fail   () { return Result(false); }
    static Result<Type> Success() { return Result(true);  }

    bool const success;
    Type const value;

    Result<Type>(bool success, Type const &v)
        : success(success)
        , value  (v)
    {
        // Empty...
    }

    operator bool() { return success; }
}; // class Result

} // namespace pw
