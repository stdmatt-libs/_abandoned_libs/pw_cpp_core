//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Debugger.hpp                                                  //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 16, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

// PixWiz - Detail
#include "Debug/Detail/DebuggerDetail.hpp"

namespace pw { namespace Debug {

#if PW_BUILD_MODE_DEBUG
    #define PW_DEBUGGER_BREAK() \
        pw::Detail::DebuggerBreak();

    bool HasDebugger();

#else // !PW_BUILD_MODE_DEBUG
    #define     PW_DEBUGGER_BREAK() do {} while(0)
    inline bool HasDebugger      () { return false; }
#endif // PW_BUILD_MODE_DEBUG

} // namespace Debug
} // namespace pw
