//
#include "Base/Discovery/OS.hpp"
#if PW_OS_OSX

// Header
#include "Debug/Detail/DebuggerDetail.hpp"
// PixWiz - Core
#include "Platform/OSX.hpp"


namespace pw { namespace Detail {

//------------------------------------------------------------------------------
void
DebuggerBreak()
{
}

//------------------------------------------------------------------------------
bool
HasDebugger()
{
    // @TODO: Missing implementation...
}


} // namespace Detail
} // namespace pw

#endif // PW_OS_OSX
