//
#include "Base/Discovery/OS.hpp"
#if PW_OS_WINDOWS

// Header
#include "Debug/Detail/DebuggerDetail.hpp"
// PixWiz - Core
#include "Platform/Win32.hpp"

namespace pw { namespace Detail

//------------------------------------------------------------------------------
void
DebuggerBreak()
{
   ::DebugBreak();
}

//------------------------------------------------------------------------------
bool
HasDebugger()
{
    return ::IsDebuggerPresent();
}
} // namespace Detail
} // namespace pw

#endif // PW_OS_WINDOWS
