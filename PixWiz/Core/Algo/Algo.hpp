//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Algo.hpp                                                      //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 12, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

// std
#include <algorithm>
#include <iterator>
#include <vector>
// PixWiz - Core
#include "Assert/Assert.hpp"



namespace pw { namespace Algo {

//----------------------------------------------------------------------------//
// Find                                                                       //
//----------------------------------------------------------------------------//
//
// Find If
//
//------------------------------------------------------------------------------
template <typename Container, typename Pred>
typename Container::iterator
FindIf(Container &container, Pred pred)
{
    return std::find_if(std::begin(container), std::end(container), pred);
}


//------------------------------------------------------------------------------
template <typename Container, typename Pred>
typename Container::const_iterator
FindIf(Container const &container, Pred pred)
{
    return std::find_if(std::begin(container), std::end(container), pred);
}


//
// Find
//
//------------------------------------------------------------------------------
template <typename Container, typename Value>
typename Container::iterator
Find(Container &container, Value const &value)
{
    return std::find(std::begin(container), std::end(container), value);
}

//----------------------------------------------------------------------------//
// Sort                                                                       //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
template <typename Container>
void
Sort(Container &container)
{
    std::sort(std::begin(container), std::end(container));
}

//------------------------------------------------------------------------------
template <typename Container, typename Pred>
void
Sort(Container &container, Pred pred)
{
    std::sort(std::begin(container), std::end(container), pred);
}


//----------------------------------------------------------------------------//
// Unique                                                                     //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
template <typename Container>
void
UniqueErase(Container &container)
{
    auto last_it = std::unique(std::begin(container), std::end(container));
    container.erase(last_it, std::end(container));
}


//----------------------------------------------------------------------------//
// Remove                                                                     //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
template <typename Container, typename Value>
bool
RemoveErase(Container &container, const Value *value)
{
    auto it = Algo::Find(container, value);
    if(it == std::end(container)) {
        return false;
    }

    container.erase(it);
    return true;
}


//----------------------------------------------------------------------------//
// Vector                                                                     //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
template <typename Type>
void
VectorAppend(std::vector<Type> &dst, std::vector<Type> const &src)
{
    dst.reserve(dst.size() + src.size());
    dst.insert(dst.end(), src.begin(), src.end());
}



} // namespace Algo
} // namespace pw
