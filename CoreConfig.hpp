//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : CoreConfig.hpp                                                //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 16, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

//
// Build Mode
//
#if !defined(PW_BUILD_MODE_RELEASE)
    #define PW_BUILD_MODE_DEBUG 1
#endif


//
// Log
//
#if !defined(PW_LOG_FATAL_SHOULD_DEBUG_BREAK)
    #define PW_LOG_FATAL_SHOULD_DEBUG_BREAK 1
#endif // !defined(PW_LOG_FATAL_SHOULD_DEBUG_BREAK)
